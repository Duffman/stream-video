var express = require('express');
var router = express.Router();
var request = require('request');
var streambuffers = require('stream-buffers');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {title: 'Express'});
});

router.get('/testVideo',function(req,res) {
  res.render('videoPage');
})

router.get('/testFile', function (req, res) {
  var range = req.headers.range;
  var positions = range.replace(/bytes=/, "").split("-");
  var start = parseInt(positions[0], 10);

  var objectPath = "http://www.w3schools.com/html/mov_bbb.mp4"
    request.get({
      url: objectPath,
      headers: {'content-type': 'video/mp4'},
      encoding: null
    }, function (err, data) {
      if (err) {
        console.log('error', err);
      }else {
        var total = data.body.length;
        var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
        var chunksize = (end - start) + 1;

        console.log("bytes " + start + "-" + end + "/" + total);
        res.writeHead(206, {
          "Content-Range": "bytes " + start + "-" + end + "/" + total,
          "Accept-Ranges" : "bytes",
          "Content-Type": 'video/mp4',
          "Content-Length": chunksize
        });
        var bodyStream = new streambuffers.ReadableStreamBuffer({
          frequency: 1,
          chunksize: 256
        });
        bodyStream.pipe(res);
        bodyStream.put(data.body);
      }
    });
});

module.exports = router;
